package com.victorlapin.flasher

object Screens {
    const val ACTIVITY_MAIN = "activity_main"
    const val ACTIVITY_SETTINGS = "activity_settings"
    const val ACTIVITY_ABOUT = "activity_about"
    const val ACTIVITY_REBOOT_DIALOG = "activity_reboot_dialog"

    const val FRAGMENT_HOME = "fragment_home"
    const val FRAGMENT_SCHEDULE = "fragment_schedule"
    const val FRAGMENT_SETTINGS = "fragment_settings"
    const val FRAGMENT_ABOUT = "fragment_about"

    const val EXTERNAL_ABOUT = "about"

    const val SERVICE_SCRIPT = "tile_service_script"

    const val RECEIVER_ALARM = "receiver_alarm"
    const val RECEIVER_BOOT_ALARM = "receiver_boot_alarm"
}